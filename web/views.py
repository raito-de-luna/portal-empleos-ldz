from django.shortcuts import render
from web.models import OfertaLaboral
from django.db.models import Q

# Create your views here.
def index(request):
    ofertas = OfertaLaboral.objects.all().values("titulo", "descripcion", "slug")  
    contexto = {
        "ofertas": ofertas
    }
    return render(request, "web/index.html", context=contexto)

def mostrar_empleo(request, slug):
    contexto = {
        "oferta": OfertaLaboral.objects.get(slug=slug)
    }
    return render(request, "web/empleos.html", context=contexto)    

def mostrar_plantilla(request, nombre):
    return render(request, f"web/{nombre}.html")

def buscar_empleos(request):
    busqueda = request.POST.get("busqueda")
    ofertas = OfertaLaboral.objects.filter(
        Q(titulo__icontains=busqueda) | Q(descripcion__icontains=busqueda)
    ).order_by("-fecha")
    print(ofertas.query)
    contexto = {
        "ofertas": ofertas,
        "busqueda": busqueda 
    }
    return render(request, "web/busqueda.html", context=contexto)  
