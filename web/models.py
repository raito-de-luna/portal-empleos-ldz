from django.db import models
from django.db.models.enums import Choices
from tinymce.models import HTMLField

class Empresa(models.Model):
    nombre = models.CharField(max_length=25)
    descripcion = models.TextField()
    telefono = models.CharField(max_length=15)
    email = models.EmailField()
    domicilio = models.CharField(max_length=50)
    url = models.URLField(blank=True)
    
    def __str__(self):
        return self.nombre


class Recruiter(models.Model):
    nombre = models.CharField(max_length=25)
    apellido = models.CharField(max_length=25) 
    telefono = models.CharField(max_length=15) 
    email = models.EmailField()
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)

    def __str__(self): 
        return f"{self.nombre} {self.apellido} - {self.empresa}"


class OfertaLaboral(models.Model):
    MODALIDADES = (
        ("PRESENCIAL", "Presencial"),
        ("REMOTO", "Remoto"),
        ("HIBRIDO", "Hibrido"),
        ("OTRO", "Otro"),
    )

    CONTRATACION = (
        ("DEPENDENCIA", "Relación de dependencia"),
        ("TEMPORAL", "Temperal"),
        ("FREELANCE", "Freelance"),
        ("CONTRACTOR", "Contractor"),
        ("OTRO", "Otro"),
    )

    JORNADA = (
        ("FULL", "Full time"),
        ("PART", "Part time"),
        ("FRANQUERO", "Franquero"),
        ("OTRO", "Otro"),
    )

    titulo = models.CharField(max_length=50)
    descripcion = HTMLField() 
    jornada = models.CharField(max_length=9, choices=JORNADA)
    modalidad = models.CharField(max_length=10, choices=MODALIDADES)
    contratacion = models.CharField(max_length=11, choices=CONTRATACION) 
    requisitos_excluyentes = HTMLField(blank=True) 
    requisitos_valorables = HTMLField(blank=True)
    requisitos_bonus = HTMLField(blank=True)
    beneficios = HTMLField(blank=True)
    salario_desde = models.CharField(max_length=10, blank=True)
    salario_hasta = models.CharField(max_length=10, blank=True)
    fecha = models.DateField()
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=50)


    def __str__(self): 
        return f"{self.empresa} - {self.titulo}"

    class Meta:
        verbose_name_plural = "Ofertas Laborales"


class Candidato (models.Model):
    nombre = models.CharField(max_length=25)
    apellido = models.CharField(max_length=25) 
    fecha_nacimiento = models.DateField()
    domicilio = models.CharField(max_length=50)
    telefono = models.CharField(max_length=15) 
    email = models.EmailField()
    linkedin = models.URLField(blank=True)
    github = models.URLField(blank=True)
    web_personal = models.URLField(blank=True)

    def __str__(self): 
        return f"{self.nombre} - {self.apellido}"


class Postulacion (models.Model):
    ESTADO = (
        ("PENDIENTE", "Pendiente"),
        ("VISTO", "Visto"),
    )
    salario_desde = models.CharField(max_length=10, blank=True)
    salario_hasta = models.CharField(max_length=10, blank=True)
    estado = models.CharField(max_length=9, choices=ESTADO)
    fecha = models.DateField(auto_now_add=True)
    candidato = models.ForeignKey(Candidato, on_delete=models.CASCADE)
    oferta_laboral = models.ForeignKey(OfertaLaboral, on_delete=models.CASCADE)

    def __str__(self): 
        return f"{self.oferta_laboral} - {self.candidato}"

    class Meta:
        verbose_name_plural = "Postulaciones"
