from django.contrib import admin
from web.models import Candidato, Empresa, OfertaLaboral, Postulacion, Recruiter


admin.site.register(Empresa)
admin.site.register(Recruiter)
admin.site.register(OfertaLaboral)
admin.site.register(Candidato)
admin.site.register(Postulacion)



