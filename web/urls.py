from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('empleos/<str:slug>', views.mostrar_empleo, name="empleo"),
    path('empleos/', views.buscar_empleos, name="buscar_empleos"),
    path('plantilla/<str:nombre>', views.mostrar_plantilla, name="plantilla"),
]